
/* Smooth Scroll */
var scroll = new SmoothScroll('a[href*="#"]');

//funcion desactivar campo url sitio web formulario modal
function disablewebinput(){
  document.getElementById("inputWebsite").disabled = true;   
}

//funcion activar campo url sitio web formulario modal
function enablewebinput(){
  document.getElementById("inputWebsite").disabled = false;   
}

function costoMensual(){
  document.getElementById("costo1").innerHTML = "$6,500";
  document.getElementById("costo2").innerHTML = "$8,500";
  var element = document.getElementById("btn-status1");
  element.classList.add("active");
  //demas botones
  var element1 = document.getElementById("btn-status2");
  element1.classList.remove("active");
  var element2 = document.getElementById("btn-status3");
  element2.classList.remove("active");
  var element3 = document.getElementById("btn-status4");
  element3.classList.remove("active");
}

function costoTrimestral(){
  document.getElementById("costo1").innerHTML = "$17,550";
  document.getElementById("costo2").innerHTML = "$22,950";
  var element = document.getElementById("btn-status2");
  element.classList.add("active");
  //demas botones
  var element1 = document.getElementById("btn-status1");
  element1.classList.remove("active");
  var element2 = document.getElementById("btn-status3");
  element2.classList.remove("active");
  var element3 = document.getElementById("btn-status4");
  element3.classList.remove("active");
}

function costoSemestral(){
  document.getElementById("costo1").innerHTML = "$33,150";
  document.getElementById("costo2").innerHTML = "$43,350";
  var element = document.getElementById("btn-status3");
  element.classList.add("active");
  //demas botones
  var element1 = document.getElementById("btn-status1");
  element1.classList.remove("active");
  var element2 = document.getElementById("btn-status2");
  element2.classList.remove("active");
  var element3 = document.getElementById("btn-status4");
  element3.classList.remove("active");
}

function costoAnual(){
  document.getElementById("costo1").innerHTML = "$62,400";
  document.getElementById("costo2").innerHTML = "$81,600";
  var element = document.getElementById("btn-status4");
  element.classList.add("active");
  //demas botones
  var element1 = document.getElementById("btn-status1");
  element1.classList.remove("active");
  var element2 = document.getElementById("btn-status2");
  element2.classList.remove("active");
  var element3 = document.getElementById("btn-status3");
  element3.classList.remove("active");
}


/* Script para el carrusel de clientes */
var swiper = new Swiper('.blog-slider', {
    spaceBetween: 30,
    effect: 'fade',
    loop: true,
    mousewheel: {
      invert: false,
    },
    // autoHeight: true,
    pagination: {
      el: '.blog-slider__pagination',
      clickable: true,
    }
  });

  (function($) {
    // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 75
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-scrolled");
    } else {
      $("#mainNav").removeClass("navbar-scrolled");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  })(jQuery); // End of use strict