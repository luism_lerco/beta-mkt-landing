function submit_cf(){
    //Validate Form
    var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+.)+[A-Z]{2,4}$/i;
    var name = $('#name').val();
    var email = $('#email').val();
    var phone = $('#phone').val();
    var position = $('#position').val();
    var companyname = $('#companyname').val();
    var state = $('#inputState').val();
    var companyturn = $('#inputCompanyTurn').val();
    var employees = $('#inputEmployees').val();
    var message = $('#message').val();
    var website = $('#inputWebsite').val();

    if (document.getElementById('inlineRadio1').checked){
        var paginaweb = document.getElementById('inlineRadio1').value;
    }
    if (document.getElementById('inlineRadio2').checked){
        var paginaweb = document.getElementById('inlineRadio2').value;
    }
    if (document.getElementById('inlineRadio3').checked){
        var redes = document.getElementById('inlineRadio3').value;
    }
    if (document.getElementById('inlineRadio4').checked){
        var redes = document.getElementById('inlineRadio4').value;
    }
    
    var companyneeds = new Array();

    //Referimos la seccion de necesidades    
    var divCN = document.getElementById('companyNeeds');

    //Referimos los checkboxes del la seccion
    var chks = divCN.getElementsByTagName("INPUT");

    // Loop and push the checked CheckBox value in Array.
    for (var i = 0; i < chks.length; i++) {
        if (chks[i].checked) {
            companyneeds.push(chks[i].value);
        }
    }

    //check if the field is empty
    if(name.trim() == ''){
        alert('Please enter your name.');
        $('#name').focus();
        return false;
    }else if(email.trim() == ''){
        alert('Please enter your email.');
        $('#email').focus();
        return false;
    }else if(email.trim() != '' && !reg.test(email)){
        alert('Please enter valid email.');
        $('#email').focus();
        return false;
    }else{
        $.ajax({
            type:'POST',
            url:'submit_form.php',
            data:'contactFrmSubmit=1&name='+name+'&email='+email+'&phone='+phone+'&position='+position+'&companyname='+companyname+'&state='+state+'&companyturn='+companyturn+'&employees='+employees+'&message='+message+'&website='+website+'&paginaweb='+paginaweb+'&redes='+redes+'&companyneeds='+companyneeds,
            beforeSend: function(){
                $('.submitBtn').attr("disabled","disabled");
                $('.modal-body').css('opacity', '.5');
            },
            success:function(msg){
                if(msg == 'ok'){
                    $('#name').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    $('.statusMsg').html('<span style="color:#00e640;">Gracias por escribirnos, en breve nos contactaremos contigo.</p>');
                }else{
                    $('.statusMsg').html('<span style="color:#f22613;">Ocurrio un error, porfavor intente más tarde.</span>');
                }
                $('.submitBtn').removeAttr("disabled");
                $('.modal-body').css('opacity', '');
            }
        });
    }
}