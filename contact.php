<?php
/*
 *  CONFIGURE EVERYTHING HERE
 */

// an email address that will be in the From field of the email.
$from = 'Beta MKT desde <contacto@betamkt.mx>';

// an email address that will receive the email with the output of the form
$sendTo = 'Beta MKT <amaranta@betamkt.mx>';

// subject of the email
$subject = 'Nuevo mensaje desde el formulario de contacto';

// form field names and their translations.
// array variable name => Text to appear in the email
$fields = array('name' => 'Nombre', 'phone' => 'Telefono', 'need' => 'Necedidad de la empresa', 'email' => 'Email', 'message' => 'Mensaje', 
'company'=>'Empresa', 'position' =>'Puesto de trabajo', 'website'=>'Sitio Web', 'content'=>'Creación de contenido', 'web_pos'=>'Posicisionamiento Web',
'sales'=>'Aumentar las Ventas', 'branding'=>'Branding', 'state'=>'Estado', 'company_line'=>'Giro de la Empresa', 'num_emp'=>'Numero de empleados',
'paginaWeb'=>'Cuenta con pagina', 'redesSociales'=>'Cuenta con redes sociales'); 

// message that will be displayed when everything is OK :)
$okMessage = '¡Recibimos tu mensaje y nos pondremos en contacto contigo!';

// If something goes wrong, we will display this message.
$errorMessage = 'Upss! Hubo un error enviando tu mensaje. Por favor intenta más tarde.';

/*
 *  LET'S DO THE SENDING
 */

// if you are not debugging and don't need error reporting, turn this off by error_reporting(0);
error_reporting(E_ALL & ~E_NOTICE);

try
{

    if(count($_POST) == 0) throw new \Exception('Form is empty');
            
    $emailText = "Tienes un nuevo mensaje del formulario de contacto\n=============================\n";

    foreach ($_POST as $key => $value) {
        // If the field exists in the $fields array, include it in the email 
        if (isset($fields[$key])) {
            $emailText .= "$fields[$key]: $value\n";
        }
    }

    // All the neccessary headers for the email.
    $headers = array('Content-Type: text/plain; charset="UTF-8";',
        'From: ' . $from,
        'Reply-To: ' . $from,
        'Return-Path: ' . $from,
    );
    
    // Send email
    mail($sendTo, $subject, $emailText, implode("\n", $headers));

    $responseArray = array('type' => 'success', 'message' => $okMessage);
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
}


// if requested by AJAX request return JSON response
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-Type: application/json');

    echo $encoded;
}
// else just display the message
else {
    echo $responseArray['message'];
}
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Beta MKT | Gracias por contactarnos</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">

   </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-dark static-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="img/logo.png" alt="Beta Marketing Logo" class="d-inline-block align-middle mr-2">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/#quienes-somos">Quienes Somos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/#clientes">Nuestros Clientes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/#servicios">Servicios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/#contacto">Contacto</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header class="hero">
        <div class="background"></div>
        <div class="background2">
            <div class="bgcut"></div>
        </div>
        <div class="container py-1">
            <div class="row h-100 align-items-center text-white py-2">
                <div class="col-md-12 text-center">
                    <h2 class="pb-3">¡Gracias por ponerte en contacto con nosotros!</h2>
                    <a name="" id="" class="btn btn-primary btn-lg" href="/" role="button">Regresar</a>
                </div>
            </div>
        </div>
    </header>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>