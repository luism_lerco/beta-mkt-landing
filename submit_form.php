<?php
if(isset($_POST['contactFrmSubmit']) && !empty($_POST['name']) && !empty($_POST['email']) && (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) && !empty($_POST['message'])){
    
    // Datos enviados desde el formulario
    $name     = $_POST['name'];
    $email    = $_POST['email'];
    $phone    = $_POST['phone'];
    $position = $_POST['position'];
    $company  = $_POST['companyname'];
    $companyturn = $_POST['companyturn'];
    $employees = $_POST['employees'];
    $state   = $_POST['state'];
    $message  = $_POST['message'];
    $website  = $_POST['website'];
    $paginaweb = $_POST['paginaweb'];
    $redes    = $_POST['redes'];
    $needs   = $_POST['companyneeds'];
    
    /*
     * Datos de Envio
     */
    $to     = 'l.miguel@lerco.mx, amaranta@betamkt.mx'; //Correo a quien envia
    $subject= 'Formulario de Diagnostico'; //Asunto del correo

    // To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    // Additional headers
    $headers .= 'From: Beta MKT<contacto@betamkt.mx>' . "\r\n";

    
    $htmlContent = '
    <html>
    <body>
    <h4>Registro del formulario de diagnostico.</h4>
    <table cellspacing="0" style="width: 300px; height: 200px;">
        <tr>
            <th>Nombre:</th><td>'.$name.'</td>
        </tr>
        <tr style="background-color: #e0e0e0;">
            <th>Email:</th><td>'.$email.'</td>
        </tr>
        <tr>
            <th>Telefono:</th><td>'.$phone.'</td>
        </tr>
        <tr style="background-color: #e0e0e0;">
            <th>Puesto:</th><td>'.$position.'</td>
        </tr>
        <tr>
            <th>Compañia:</th><td>'.$company.'</td>
        </tr>
        <tr style="background-color: #e0e0e0;">
            <th>Estado:</th><td>'.$state.'</td>
        </tr>
        <tr>
            <th>Giro:</th><td>'.$companyturn.'</td>
        </tr>
        <tr style="background-color: #e0e0e0;">
            <th>Numero empleados</th><td>'.$employees.'</td>
        </tr>
        <tr>
            <th>¿Tiene sitio?:</th><td>'.$paginaweb.'</td>
        </tr>
        <tr style="background-color: #e0e0e0;">
            <th>Url sitio:</th><td>'.$website.'</td>
        </tr>
        <tr>
            <th>¿Tiene redes?:</th><td>'.$redes.'</td>
        </tr>
        <tr style="background-color: #e0e0e0;">
            <th>Necesidades:</th><td>'.$needs.'</td>
        </tr>
        <tr>
            <th>Adicional:</th><td>'.$message.'</td>
        </tr>
    </table>
    </body>
    </html>';
    
    // Set content-type header for sending HTML email
    //$headers = "MIME-Version: 1.0" . "rn";
    //$headers .= "Content-type:text/html;charset=UTF-8" . "rn";
    
    
    
    // Send email
    if(mail($to,$subject,$htmlContent,$headers)){
        $status = 'ok';
    }else{
        $status = 'err';
    }
    
    // Output status
    echo $status;die;
}